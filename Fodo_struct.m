function [ x_r ] = Fodo_struct(x_r, L, K, beta, gama, op_mode  )
    
    switch op_mode
        case 1
            
            %focus drift defocus drift focus.
            x_r(:,2) = qf_tran(L/2, K, beta, gama,1)*x_r(:,1); %focus
            x_r(:,3) = ds_tran(L, beta, gama)*x_r(:,2); % drift space
            x_r(:,4) = qf_tran(L, -K, beta, gama, 0)*x_r(:,3); %defocus
            x_r(:,5) = ds_tran(L, beta, gama)*x_r(:,4); % drift space
            x_r(:,6) = qf_tran(L/2, K, beta, gama,1)*x_r(:,5);% focus.
        case 2
            %defocus drift focus drift defocus.
            x_r(:,2) = qf_tran(L/2, -K, beta, gama,0)*x_r(:,1);
            x_r(:,3) = ds_tran(L, beta, gama)*x_r(:,2);
            x_r(:,4) = qf_tran(L, K, beta, gama, 1)*x_r(:,3);
            x_r(:,5) = ds_tran(L, beta, gama)*x_r(:,4);
            x_r(:,6) = qf_tran(L/2, -K, beta, gama,0)*x_r(:,5);
        otherwise
            display('work in progress');
    end
end

