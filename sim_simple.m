% the script is used for simulating the linear accleration of a particle
% across the space. As of now, simple simulation involves only drift space and
% quadoples

%functions used: Fodo_struct -- sets up the fodo_struct.
%                ds_tran,    -- creates the drift space transfer matrix.
%                qf_tran     -- creates the quadople transfer matrix.
%                continous_drift -- creates a continuous drift space.
%author : Arun Jeevaraj.

%% clear the work environment.
clear all;
clc
close all;

%% simulation settings.

sim_mode = 4;       % 0 - for just drift space.
                    % 1 - for drift space and quadople.
                    % 2
                    % 3
                    % 4 - the optimized version.

Num_of_particles = 1000;
% total number of transfer matrix operations done per particle.
max_tran_op = 5; % transfer points in each cell.
max_cells = 10; % to repeat the same 

% the  particles are denoted by xr vector. a column vector of 
% xr denotes the particle after each transfer. xr is a 3D vector space.
xr=zeros(6, max_tran_op*max_cells+1, Num_of_particles);
% create the s_cale for plotting.
s_scale = zeros(1,max_tran_op*max_cells+1);
for cell_i = 1: max_cells
  s_scale(5*(cell_i-1)+2:5*(cell_i-1)+6)=  4*(cell_i-1)+[.5,1.5,2.5,3.5,4];
end

%% setup drift space parameters and constants.
M_PROT = 938.2720813e6;                 %938.27 Mega elecronvolts/c^2. mass of e-
KE_PROT = 1e9;                          %1G electronvolt Kinetic energy.
L = 1;                                  %1 meter of drift space.
C = 299792458;                          %Speed of light. constant.
gama = KE_PROT/(M_PROT * C^2);
beta = sqrt(1-1/gama^2);
K = 1;                               

% initial condition of 1000 particle.
% normally distributed random number between a and b.
b = 1e-3; a = -b;
xr(:,1,:) = (b-a)*rand(size(xr,1),1,size(xr,3))+a;
%xr(2,:,:) = .01+xr(2,:,:);
%xr(4,:,:) = .01+xr(4,:,:);
%xr(5,:,:) = zeros(1,size(xr,2),size(xr,3));
%xr(6,:,:) = zeros(1,size(xr,2),size(xr,3));
R_qffoc = qf_tran(L/2, K, beta, gama,1);
R_qfdef = qf_tran(L, -K, beta, gama,0);
R_drift = ds_tran(L, beta, gama);

% check the determinant is one.
det_qff = det(R_qffoc);
det_qfd = det(R_qfdef);
det_dss = det(R_drift);
%% run simulation.
switch sim_mode
    case 0 
       %% continuous drift space.
        R_drift = ds_tran(L, beta, gama);
        xr = continuous_drift(xr,R_drift,5);
        xyz = xr(1:2:end,:);
        % plot it in a 3d space.
        figure;
        stem3(xyz(1,:), xyz(2,:), xyz(3,:));
    case 1
        %% focus drift defocus drift focus. op_mode = 1 else 2.
        %creates a 3D. Plot. 
        op_mode = 1;
        figure; hold on;
        for p_i = 1: Num_of_particles
            xr_p = Fodo_struct(xr(:,:,p_i), L, K, beta, gama, op_mode );
            %get the xyz cordinates of the particle at each instances.
            xyz = xr_p(1:2:end,:);
            % plot it in a 3d space.
            stem3(xyz(1,:), xyz(2,:), xyz(3,:));
           
        end
        xlabel('x-axis');
        ylabel('y-axis');
        zlabel('z-axis');
        title('position of particle after each transfer')
 
      
    case 3
        %% non optimal implementation.
            op_mode = 1;
            figure; hold on;
        for cell_i = 1: 100
            
            for p_i = 1: Num_of_particles
                xr_p = Fodo_struct(xr(:,:,p_i), L, K, beta, gama, op_mode );
                %get the xyz cordinates of the particle at each instances.
                xyz = xr_p(1:2:end,:);
                % plot it in a 3d space.
                %  stem3(xyz(1,:), xyz(2,:), xyz(3,:));
                stem(4*cell_i+[0,.5,1.5,2.5,3.5,4],xyz(1,:))
                xr(:,1,p_i)= xr_p(:,6);
            end  
            cell_i
        end
            xlabel('S distance L');
            ylabel('X');
       % zlabel('z-axis');
       % title('position of particle after each transfer')
    case 4
        %% creates the plot of each particle parameter against S.
      for i_par = 1:Num_of_particles
            xr(:,2,i_par) = R_qffoc*xr(:,1,i_par);              % focus
            xr(:,3,i_par) = R_drift*xr(:,2,i_par);              % drift space
            xr(:,4,i_par) = R_qfdef*xr(:,3,i_par);              % defocus
            xr(:,5,i_par) = R_drift*xr(:,4,i_par);              % drift space
            xr(:,6,i_par) = R_qffoc*xr(:,5,i_par);              % focus.
        for cell_i = 1: max_cells-1
            xr(:,cell_i*5+2,i_par) = R_qffoc*xr(:,cell_i*5+1,i_par); % focus
            xr(:,cell_i*5+3,i_par) = R_drift*xr(:,cell_i*5+2,i_par); % drift space
            xr(:,cell_i*5+4,i_par) = R_qfdef*xr(:,cell_i*5+3,i_par); % defocus
            xr(:,cell_i*5+5,i_par) = R_drift*xr(:,cell_i*5+4,i_par); % drift space
            xr(:,cell_i*5+6,i_par) = R_qffoc*xr(:,cell_i*5+5,i_par); % focus.
        end
      end
      % plotting
      figure(1); hold on;
      title('x vs s');
      xlabel('S in L units');
      ylabel('X coordinate of particle');
      for i_par = 1:Num_of_particles
           stem(s_scale,xr(1,:,i_par));
      end
      figure(2); hold on;
      title('y vs s');
      xlabel('S in L units');
      ylabel('Y coordinate of particle');
      for i_par = 1:Num_of_particles
           stem(s_scale,xr(3,:,i_par));
      end
      figure(3); hold on;
      title('z vs s');
      xlabel('S in L units');
      ylabel('Z coordinate of particle');
      for i_par = 1:Num_of_particles
           stem(s_scale,xr(5,:,i_par));
      end
      figure(4); hold on;
      title('px vs s');
      xlabel('S in L units');
      ylabel('pX coordinate of particle');
      for i_par = 1:Num_of_particles
           stem(s_scale,xr(2,:,i_par));
      end
      figure(5); hold on;
      title('py vs s');
      xlabel('S in L units');
      ylabel('py coordinate of particle');
      for i_par = 1:Num_of_particles
           stem(s_scale,xr(4,:,i_par));
      end
      figure(6); hold on;
      title('pz vs s');
      xlabel('S in L units');
      ylabel('pz coordinate of particle');
      for i_par = 1:Num_of_particles
           stem(s_scale,xr(6,:,i_par));
      end
      
      
    otherwise
        %% for future.
        display('watch out at sim_mode');
end

