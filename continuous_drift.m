function [ x_r ] = continuous_drift( x_r, r_drift, n_ds )
    for i = 2: size(x_r,2)
        x_r(:,i) = r_drift*x_r(:,i-1);
    end
end

